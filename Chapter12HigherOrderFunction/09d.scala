val a = Array("HelloL", "forMulaOne1", "BlackHwaks")

val b = a.map(_.length)

def corresponds[A,B](a:Seq[A],b:Seq[B],f:(A,B)=> Boolean)={
  (a zip b).map(x=>f(x._1,x._2)).count(!_)==0


}

val isResult = corresponds(a,b,(s:String,x:Int)=>s.length==x)

println(isResult) //true

println((a zip b).map(x=>(x._1,x._2)).toSeq)

/*
WrappedArray((HelloL,6), (forMulaOne1,11), (BlackHwaks,10))
Davids-MacBook-Pro:byMe davidtan$

 */

