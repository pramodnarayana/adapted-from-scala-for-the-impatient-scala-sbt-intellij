


trait Animal {
  val name: String
  def words: String = ""
  def say = println("%s: %s".format(name, words))
}

trait Endothermy // abstract

trait Mammmal extends Animal with Endothermy

class Dog extends Mammmal {
  val name : String = "Scoobyy"
  override val words :String ="ScoobyDooooo"
}

class Cat extends Mammmal{
  val name :String = "Tmi"
  override val words:String ="MewoZ"
  override def say = {print("Cat ")
  super.say}
}

val scooby = new Dog
val timmy = new Cat

scooby.say

println()
timmy.say

