
def prod(s: String) = {
	var res: Long = 1;
	for( ch <- s ) res *= ch.toLong
	res
}


println(prod("Hello"))
println("Hello".foldLeft(1L)(_ * _.toLong))

//scala -Xnojline < 06.scala
//scala> 9415087488
//
//scala> 9415087488
