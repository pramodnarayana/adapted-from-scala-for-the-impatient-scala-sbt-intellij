import scala.util.Random

val x = new Array[Int](10).map(x => Random.nextInt(100))

println(x.mkString(","))

//val max = x.reduceLeft((x,next)=> {println(x);(x+next);})//((x, y) => if (x > y) y else x)

val max = x.reduceLeft((x, next) => if (x > next) x else next)

println(max)

/*
Davids-MacBook-Pro:byMe02 davidtan$ scala 02.scala
61,17,62,23,18,74,76,0,97,98


98

 */