import scala.collection.immutable.TreeMap



def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ",", ")"))

val source = io.Source.fromFile("gpl.txt", "UTF-8")
val tokens = source.mkString.split("\\s+")

var hist = new TreeMap[String, Int]

tokens map { token => hist = hist + (token -> (hist.getOrElse(token, 0) + 1))}
prn(hist)
//println(tokens.map(token => (token, 1)).groupBy(_._1).map(x=>(x._1,x._2)).mkString(","))
