
def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

val gizmos = Map("iPad3" -> 700, "iPhone 5" -> 600, "MacBook Pro Retina" -> 2000)

val discount = for( (k,v) <- gizmos ) yield (k, v * 0.9)

prn(gizmos)
prn(discount)

/*
Davids-MacBook-Pro:Chapter04MapandTuples davidtan$ scala 01.scala
Map3(iPad3 -> 700, iPhone 5 -> 600, MacBook Pro Retina -> 2000)
Map3(iPad3 -> 630.0, iPhone 5 -> 540.0, MacBook Pro Retina -> 1800.0)

 */