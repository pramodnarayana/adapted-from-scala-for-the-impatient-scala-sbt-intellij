
object Conversations {
	def inchesToSantimeters(value: Double) = value * 2.54
	def gallonsToLiters(value: Double) = value * 3.78541178
	def milesToKilometers(value: Double) = value * 1.609344
}

println("%f inch = %f santimeters".format(1.0, Conversations.inchesToSantimeters(1.0)))
println("%f gallon = %f liters".format(1.0, Conversations.gallonsToLiters(1.0)))
println("%f mile = %f kilometers".format(1.0, Conversations.milesToKilometers(1.0)))

/*
Davids-MacBook-Pro:Chapter06Objects davidtan$ scala 01.scala
1.000000 inch = 2.540000 santimeters
1.000000 gallon = 3.785412 liters
1.000000 mile = 1.609344 kilometers

 */