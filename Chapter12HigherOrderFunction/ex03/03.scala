def factorial(x: Int): Int = (1 to x) reduceLeft ((first, next) => first * next)

println(factorial(5)) //120

def factorial2(x: Int): Int = (1 to x) reduceLeft (_ * _)
println(factorial2(5)) //120