

def filterZero(_list: List[Int]): List[Int] = _list match {
  case Nil => Nil
  case head :: tail => if (head != 0) head :: filterZero(tail) else filterZero(tail)
}

val a = List(0,2,3,5,6,0,0,8,9,0,10,12)

println(filterZero(a))//List(2, 3, 5, 6, 8, 9, 10, 12)


def filterZero02(ints:List[Int]):List[Int]={
  ints.filterNot{ _== 0}
}

println(filterZero02(a))//List(2, 3, 5, 6, 8, 9, 10, 12)