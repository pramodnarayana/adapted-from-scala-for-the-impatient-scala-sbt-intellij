

def adjusttoPair(f: (Int, Int) => Int) = (x: (Int, Int)) => f(x._1, x._2)

val x = adjusttoPair(_ * _)(6, 7)

println(x) //42

val pairs = (1 to 10) zip (20 to 30)

println(pairs)

val y = pairs.map(adjusttoPair(_*_))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 07b.scala
42
Vector((1,20), (2,21), (3,22), (4,23), (5,24), (6,25), (7,26), (8,27), (9,28), (10,29))
Vector(20, 42, 66, 92, 120, 150, 182, 216, 252, 290)
Davids-MacBook-Pro:byMe davidtan$
 */

println(y)