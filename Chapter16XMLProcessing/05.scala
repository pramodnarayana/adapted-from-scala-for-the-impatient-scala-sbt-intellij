//5.

// scala -classpath ./*.jar 05.scala

import scala.xml.XML

val parser = (new org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl).newSAXParser
val html = XML.withSAXParser(parser).load(new java.net.URL("http://autopeople.ru/"))
val src = (html \\ "img").flatMap(_.attributes("src"))

println(src.mkString("\n"));


/*
Davids-MacBook-Pro:Chapter16XMLProcessing davidtan$ scala -classpath ./*.jar 05.scala
/templates/img/top/guest.jpg
/templates/img/design/buttons/open_theme.png
/templates/img/design/buttons/add_opinion.png
/thumb/bigindex_cover/65/2370465/rrrrr_rsrssrrrs.jpg
/thumb/bigindex_cover/08/2369708/rsrrssrr_rsrssrsr.jpg
http://img.yandex.net/i/service/wdgt/yand-add-b.png
/thumb/bigindex_cover/58/2371958/550.jpg
/thumb/bigindex_cover/97/2371697/550.jpg
/templates/img/ico_twitter.gif
/templates/img/ico_livejournal.gif
//mc.yandex.ru/watch/1571693
Davids-MacBook-Pro:Chapter16XMLProcessing davidtan$

 */