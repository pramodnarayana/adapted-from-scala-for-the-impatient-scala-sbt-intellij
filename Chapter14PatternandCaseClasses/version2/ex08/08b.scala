/*
8.


 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(ops:Char,tree: BinaryTree*) extends BinaryTree


def eval(t:BinaryTree):Int= t match{
  case Node(c,leafs @_*)=>c match{
    case '+'=>leafs.map(eval _).sum
    case '-'=> - leafs.map(eval _).sum
    case '*'=> leafs.map(eval _).product

  }
  case Leaf(x)=>x
}

val x = Node('+', Node('*', Leaf(3), Leaf(8)), Leaf(2), Node('-', Leaf(5)))

println(x)

/*
    +
    /|\
  *  2  -
 / \    |
3  8    5

has value (3 × 8) + 2 + (–5) = 21.
 */

println(eval(x)) //21