

val source = io.Source.fromFile("02.txt")
var count: Int = 0
var column: Int = 4

for (ch <- source) ch match {
  case '\t' => {
    print(" " * (column - count % column))
    count = 0 //reset
  }
  case '\n' => {
    print(ch)
    count = 0 //reset
  }
  case _ => {
    print(ch)
    count += 1
  }

}

/*
1   2   4   8
aaa 333 83838383838 888Davids-MacBook-Pro:byMe davidtan$

 */