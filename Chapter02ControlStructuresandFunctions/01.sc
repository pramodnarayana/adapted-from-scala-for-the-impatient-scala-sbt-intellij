def signum(n: Int) = {
  if (n > 0) 1
  else if (n < 0) -1
  else 0;
}

println(signum(5));
println(signum(-5));
println(signum(0));
//1
//res0: Unit = ()
//-1
//res1: Unit = ()
//0
//res2: Unit = ()

def signum2(n: Int) = n match {
  case _ if (n > 0) => 1
  case _ if (n < 0) => -1
  case _ => 0
}
println(signum2(5));
println(signum2(-5));
println(signum2(0));




//1
//res3: Unit = ()
//-1
//res4: Unit = ()
//0
//res5: Unit = ()


//Q2
{}

// Answer type Unit
//Q3

var x = {}
var y =0
x=y=1

//x: Unit = ()
//y: Int = 0
//x: Unit = ()
