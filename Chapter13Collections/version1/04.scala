import scala.collection.immutable.{Set, Map, Seq}


val a = Array("Tom", "Fred", "Harry")
val b = Map("Tom" -> 3, "Dick" -> 4, "Harry" -> 5)

def existIn(a: Array[String], b: Map[String, Int]) = {

  a.toList.map(b.get(_)).flatMap(x=>x)
}

println(existIn(a,b))

def getIntegersThatMapped(strings:Array[String], map: Map[String,Int]):Set[Int] = {
  val intersection = map.keySet.intersect(strings.toSet)
  val result = intersection.map{ map.get(_).get }
  ( result )
}

println(getIntegersThatMapped(a,b))