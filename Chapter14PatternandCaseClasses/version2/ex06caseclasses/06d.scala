/*
6.

June 01, 2015
 */

sealed abstract class BinaryTree
case class Leaf(value: Int) extends BinaryTree
case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

private def leafSumImp(tree:BinaryTree):Int = tree match{
  case Leaf(x)=>x
  case Node(l,r)=> leafSumImp(l)+leafSumImp(r)
}
def leafsum(t:BinaryTree):Int ={
  val x = leafSumImp(t)
  x
}
val tree01 = Node(Leaf(12), Node(Node(Leaf(3), Leaf(8)), Node(Leaf(5), Leaf(9))))

println(tree01)

println(leafsum(tree01))