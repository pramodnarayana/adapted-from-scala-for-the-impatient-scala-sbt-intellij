

val str = io.Source.fromFile("10.txt").mkString

def printmsg(s:String)(block: =>Unit)={
  val start = System.currentTimeMillis()
  block
  val end = System.currentTimeMillis()
  println("The time taken is %d ", end-start)
}

printmsg("This is immtualbe "){
  val freq = str.map(c=>(c,1)).groupBy(_._1).map(x=>(x._1,x._2.length))
  println(freq.toSeq.sorted)
}