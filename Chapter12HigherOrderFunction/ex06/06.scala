

def largest(fun: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(x => (x, fun(x))).reduceLeft((l, r) => if (l._2 > r._2) l else r)
}

val result = largest(x => 10 * x - x * x, 1 to 10)

println(result) //(5,25)
