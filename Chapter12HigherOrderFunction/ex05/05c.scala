
def largest(fun: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(fun(_)).max
}

//c: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,9), (2,16), (3,21), (4,24), (5,25), (6,24), (7,21), (8,16), (9,9), (10,0))

val c= (1 to 10).map(x => (x,(x * 10 - x * x)))


println(c.mkString(","))

val a = largest(x => (10 * x - x * x), (1 to 10))
println(a)

/*
(1,9),(2,16),(3,21),(4,24),(5,25),(6,24),(7,21),(8,16),(9,9),(10,0)
25

 */