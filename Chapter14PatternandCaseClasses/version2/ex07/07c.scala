/*
7.
 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(tree:BinaryTree*) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(tr @_*) =>tr.map(leafsum _).sum
  case Leaf(x) => x
}

val data = Node(Node(Leaf(1), Leaf(5)), Node(Leaf(3), Leaf(9)))

println(leafsum(data))//18