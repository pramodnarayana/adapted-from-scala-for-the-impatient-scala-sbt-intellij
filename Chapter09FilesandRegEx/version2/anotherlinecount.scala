def findWord(file: String, wordToFind: String) {
  val src = io.Source.fromFile("mary.txt")
  for {
    (line, idx) <- src.getLines.zipWithIndex
    if line.contains(wordToFind)
  } {
    println(f"$idx%02d: " + line)
  }
}

