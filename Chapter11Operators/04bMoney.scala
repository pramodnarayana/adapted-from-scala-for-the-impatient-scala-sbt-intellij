/* DOLLAR AND SENSE */

class Money(d: BigInt, c: BigInt) extends Ordered[Money] {
  val dollars = d + c / 100
  val cents = c % 100

  def toCents(): BigInt = dollars * 100 + cents

  def fromCents(cents: BigInt) = new Money(cents / 100, cents % 100)

  override def toString() = "%d.%d".format(dollars, cents)

  override def compare(that: Money) = toCents.compare(that.toCents)

  def +(that: Money) = fromCents(this.toCents + that.toCents)
  def -(that: Money) = fromCents(this.toCents - that.toCents) //left minus right

}

object Money{
  def apply(d:Int,c:Int)= new Money(d,c)
}

val x = new Money(1,75)
val y = new Money(0,50)

//println(x)
println(x+y)
println(x<y)
println(x-y)