/**
 * Created by davidtan on 2/20/15.
 */

def product(s: String) = {
  var result: Long = 1
  for (ch <- s) result *= ch.toLong
  result
}

print(product("Hello"))

print("Hello".foldLeft(1L)(_*_.toLong))

/*
Davids-MacBook-Pro:Chapter02 davidtan$ scala -Xnojline < 06b.scala
Welcome to Scala version 2.11.4 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_25).
Type in expressions to have them evaluated.
  Type :help for more information.

scala>      |      |      |      |      |      |      |      | product: (s: String)Long

scala>
  scala> 9415087488
scala>
  scala> 9415087488
scala> :quit
Davids-MacBook-Pro:Chapter02 davidtan$
*
*/
