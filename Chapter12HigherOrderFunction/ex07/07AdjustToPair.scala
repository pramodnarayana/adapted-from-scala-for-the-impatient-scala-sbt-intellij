

def adjustToPair(fun: (Int, Int) => (Int)) = (x: (Int, Int)) => fun(x._1, x._2)

val x = adjustToPair(_ * _)(6, 7)

println(x) //42

val pairs = (1 to 10) zip (21 to 30)
println("Pairs ", pairs)

val result = pairs.map(adjustToPair(_ + _))

println(result) //Vector(22, 24, 26, 28, 30, 32, 34, 36, 38, 40)
/*
(Pairs ,Vector((1,21), (2,22), (3,23), (4,24), (5,25), (6,26), (7,27), (8,28), (9,29), (10,30)))

Vector(22, 24, 26, 28, 30, 32, 34, 36, 38, 40)

 */