import scala.collection.immutable.HashMap




val str = io.Source.fromFile("10.txt").mkString

// mutable
val freq01 = new scala.collection.mutable.HashMap[Char, Int]
for (c <- str) freq01(c) = freq01.getOrElse(c, 0) + 1
//println(freq01.toSeq.sorted)


val freq02 = str.map(c => (c, 1)).groupBy(_._1).map(x => (x._1, x._2.length))
println(freq02.toSeq)
/*
k-Pro:byMe davidtan$ scala 10b.scala
ArrayBuffer((E,122), (e,3104), (X,3), (s,1576), (x,53), (
,674), (8,2), (4,5), (n,1803), (.,218), (9,4), (N,99), (j,27), (y,598), (T,144), (Y,48),
(t,2300), (J,1), (<,10), (u,764), (U,60), (f,663), (F,46), (A,124), (a,1793), (5,5), (m,623),
(M,33), (`,4), (),60), (I,129), (i,2037), ( ,5835), (",82), (-,24), (,,313), (;,17), (v,314),
(G,69), (6,8), (1,28), (V,13), (q,32), (Q,3), (L,141), (',24), (b,300), (g,456), (B,22), (l,800),
 (P,104), (p,672), (0,14), (2,13), (C,78), (H,46), (c,1087), (W,23), (h,1013), ((,45), (7,8),
  (r,2073), (K,3), (w,392), (:,11), (R,106), (3,9), (k,174), (O,94), (/,20), (D,49), (>,10),
   (o,2505), (z,11), (S,104), (d,870))
Davids-MacBook-Pro:byMe davidtan$
 */

//http://markusjais.com/scalas-parallel-collections-and-the-aggregate-method/

val freq03 = str.par.aggregate(new HashMap[Char, Int])(
  (x, c) => x + (c -> (x.getOrElse(c, 0) + 1)),
  (map1, map2) => map1 ++ map2.map { case (k, v) => k -> (v + map1.getOrElse(k, 0)) }

)

println(freq03.toSeq.sorted)