

val l_ist = List(1, 2, 3, 4, 5)

val a = (l_ist :\ List[Int]())(_ :: _) // foldLeft

println(a)
//val a2 = l_ist.foldLeft(0)(_ :: _)

//println(a2)
val b = (List[Int]() /: l_ist)(_ :+ _) //foldRight

println(b)

val rev = (List[Int]() /: l_ist)((x, y) => y :: x)

println(rev)

/*
List(1, 2, 3, 4, 5)
List(1, 2, 3, 4, 5)
List(5, 4, 3, 2, 1)

 */