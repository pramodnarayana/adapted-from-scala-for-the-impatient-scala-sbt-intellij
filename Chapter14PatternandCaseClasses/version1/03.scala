/**
 * Created by davidtan on 4/19/15.
 */


def swapfirsttwo(arr: Array[Int]) = arr match {
  case Array(x, y, rest@_*) => Array(y, x) ++ rest
  case _ => arr
}

val sample = Array(1,3,5,7)
println(sample.mkString(","))
val result = swapfirsttwo(sample)

println(result.mkString(","))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 03.scala
1,3,5,7
3,1,5,7

 */
