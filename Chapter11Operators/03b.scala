import Math._

class Fraction(n: Int, d: Int) {
  private val num: Int = if (d == 0) 1 else n * sign(d) / gcd(n, d);
  private val den: Int = if (d == 0) 0 else d * sign(d) / gcd(n, d);

  override def toString = num + "/" + den

  def sign(a: Int) = if (a > 0) 1 else if (a < 0) -1 else 0

  def gcd(a: Int, b: Int): Int = if (b == 0) abs(a) else gcd(b, a % b)

  def +(that: Fraction) ={
     new Fraction(this.num*that.den+ this.den*that.num, this.den*that.den)
  }
}
object Fraction{
  def apply(a :Int,b:Int)= new Fraction(a,b)
}
val f1 = Fraction(3,4)
println(f1) //3/4

val f2 = Fraction(2,5)


println(f1+f2)  //23/20