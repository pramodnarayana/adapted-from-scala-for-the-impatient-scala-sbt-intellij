import scala.collection.immutable.TreeMap



def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getName + "(", ",", ")"))

val source = io.Source.fromFile("gpl.txt", "UTF-8")

val tokens = source.mkString.split("\\s+")

//println(prn(tokens))

var freq = new TreeMap[String, Int]

tokens foreach { token =>
  freq = freq + (token -> (freq.getOrElse(token, 0) + 1))
  //println(token)

}

prn(freq)
