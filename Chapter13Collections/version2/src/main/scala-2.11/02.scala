import scalaz._





val names = Map("Sidney" -> 1, "Paul" -> 1, "Jacob" -> 7)
val moreNames = Map("Sidney" -> 1, "Paul" -> 5, "Nick" -> 2)

val mergedMap2 = (names.toSeq ++ moreNames.toSeq)
  .groupBy{case(name,amount) => name}
  .mapValues(person => person.map{ case(name,amount) => amount}.toList)

//val mergedMap3 = names |+| moreNames

//val mergedMap4 = names.map(p => p._1 -> List(p._2)) |+| moreNames.map(p => p._1 -> List(p._2))

//println(mergedMap3)
//
//println("--- 4 here ")
//
//println(mergedMap4)