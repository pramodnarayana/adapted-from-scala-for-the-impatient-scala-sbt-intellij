

import collection.immutable.List

abstract class Item {
  def description: String
  def price: Double
  override def toString =  "%s(%s: %f)".format(this.getClass.getSimpleName, description, price)
}

class SimpleItem(
                override val description:String,
                override val price:Double
                  ) extends Item

class Bundle extends Item{
  private var items: List[Item] = List()

  def add(item:Item)= {items = item :: items}
  def price : Double = items.map(_.price).sum
  def description : String = items.map(_.description).mkString(",")

}

var l:List[Item] = List(new SimpleItem("asdfg",1088), new SimpleItem("asdfgh",789))

val b = new Bundle
b.add(new SimpleItem("sdfg",900))
b.add(new SimpleItem("cdfghv",103))

l = b :: l

l.foreach(println(_))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 04c.scala
Bundle(cdfghv,sdfg: 1003.000000)
SimpleItem(asdfg: 1088.000000)
SimpleItem(asdfgh: 789.000000)
Davids-MacBook-Pro:byMe davidtan$

 */

