import BigInt.probablePrime
import util.Random

println(probablePrime(100, Random))


/*
Davids-MacBook-Pro:Chapter01 davidtan$ scala -Xnojline < 6.scala
Welcome to Scala version 2.11.4 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_25).
Type in expressions to have them evaluated.
Type :help for more information.

scala> import BigInt.probablePrime

scala> import util.Random

scala>
scala> 778474036742855281670942739907

scala> :quit

 */