import scala.collection.mutable._

def indexes(s: String) = {
	var res = new HashMap[Char, LinkedHashSet[Int]]()

	for((c, i) <- s.zipWithIndex) {
		val set = res.getOrElse(c.toChar, new LinkedHashSet[Int])
		set += i
		res(c.toChar) = set 
	}

	res
}

val x = indexes("Missisipi")
println(x)

/*
April 12, 2015

Davids-MacBook-Pro:Chapter13Collections davidtan$ scala 01.scala
Map(M -> Set(0), s -> Set(2, 3, 5), p -> Set(7), i -> Set(1, 4, 6, 8))

 */