object Suits extends Enumeration {
	type Suits = Value
	val Spade = Value("♠")
	val Club = Value("♣")
	val Heart = Value("♥")
	val Diamond = Value("♦")
}

println(Suits.values);


/*
Davids-MacBook-Pro:Chapter06Objects davidtan$ scala 06.scala
Suits.ValueSet(♠, ♣, ♥, ♦)

 */