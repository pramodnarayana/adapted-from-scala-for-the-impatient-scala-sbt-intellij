/*
8.


 */
sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(op: Char, tree: BinaryTree*) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(op,leafs@_*) => op match{
    case '+'=>leafs.map(leafsum _).sum
    case '-'=> -leafs.map(leafsum _).sum
    case '*'=>leafs.map(leafsum _).product
}
  case Leaf(x) => x
}

val x = Node('+', Node('*', Leaf(3), Leaf(8)), Leaf(2), Node('-', Leaf(5)))
//3*8+2-5
println(leafsum(x)) //21