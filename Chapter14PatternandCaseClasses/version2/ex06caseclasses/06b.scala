/*

6.



May 23, 2015
 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(l, r) => leafsum(l) + leafsum(r)
  case Leaf(x) => x
}


val data = Node(Node(Leaf(1), Leaf(5)), Node(Leaf(3), Leaf(9)))

println(leafsum(data))//18