/*


def f(x: Double) = if (x >= 0) Some(sqrt(Double)) else None
def g(x: Double) = if (x != 1) Some(1 / (x - 1)) else None
val h = compose(f, g)

Then h(2) is Some(1), and h(1)

and h(0) are None.

see compose http://ramdajs.com/docs/#compose

Returns function
A new function which represents the result of calling each of the input functions, passing the result of
each function call to the next, from right to left.

https://github.com/ramda/ramda/blob/bbacd095f3ab4e21c199d88a98df51eb61eb2c93/src/internal/_createComposer.js
 */


type T = Double => Option[Double]

def compose(f: T, g: T): T = {
  (x: Double) => f(x) match {
    case Some(x) => g(x)
    case None => None
  }
}
def f(x: Double) = if (x >= 0) Some(Math.sqrt(x)) else None
def g(x: Double) = if (x != 1) Some(1 / (x - 1)) else None
val h = compose(f, g)

println(h(4))
println(h(16))
println(h(1))
println(h(0))

/*
Some(1.0)
None
Some(-1.0)

 */
// this is wrong
def compose2(f: Double=>Option[Double], g: Double=>Option[Double]): Double=>Option[Double] = {
  g.apply(_) match {
    case Some(x) => f.apply(x)
    case None => None
  }
}
val h2 = compose2(f, g)

println(h2(4))
println(h2(16))
println(h2(1))
println(h2(0))