

def indexes(s: String) = {
  s.zipWithIndex.

    groupBy((_._1)).
    // make pretty
    map(x => (x._1, x._2.map(_._2).toList))
}

val a = "Mississippi"

println(indexes(a))

/*
s.zipWithIndex
Vector((M,0), (i,1), (s,2), (s,3), (i,4), (s,5), (s,6), (i,7), (p,8), (p,9), (i,10))

groupBy((_._1))
Map(M -> Vector((M,0)), s -> Vector((s,2), (s,3), (s,5), (s,6)), p -> Vector((p,8), (p,9)), i -> Vector((i,1), (i,4), (i,7), (i,10)))

 Final
 Map(M -> List(0), s -> List(2, 3, 5, 6), p -> List(8, 9), i -> List(1, 4, 7, 10))


 */