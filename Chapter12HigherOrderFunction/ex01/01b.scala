def values(f:Int=>Int, low:Int, high:Int)={
  (low to high).map(x=>(x,f(x)))
}

println(values(x=>x*x,1,10))


def values2(fun: (Int) => Int, low: Int, high: Int) = {
  for (i <- low to high) yield Pair(i, fun(i))
}
println(values2(x=>x*x,1,10))
/*
Davids-MacBook-Pro:byMe davidtan$ scala 01b.scala
Vector((1,1), (2,4), (3,9), (4,16), (5,25), (6,36), (7,49), (8,64), (9,81), (10,100))
 */