
def mkStringz[T](s: Seq[T], sep: Char = ','): String = {
  s.map(_.toString).reduceLeft(_.toString + sep + _.toString)
}

val a = Array("TOm", "Marry", "John")

println(mkStringz(a))