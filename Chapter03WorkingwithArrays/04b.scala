
def positiveThenNegative(arr:Array[Int]):Array[Int]= {
  arr.filter(_>0) ++ arr.filter(_<=0)
}
val x = Array(1,4,5,-10,100,-1,12,11,0,-50)

val front = x.filter(_>0)


println(positiveThenNegative(x).mkString("Array(",",",")")) //Array(1,4,5,100,12,11,-10,-1,0,-50)

