
private def leafSumImpl(total:Int, nodes :List[Any]):Int = nodes match {
  case List()=> total
  case head :: tail =>
    head match {
      case x:Int => leafSumImpl(x+total,tail)
      case y:List[Any]=> leafSumImpl(total,y:::tail)
      case _ => total

    }
}



def leafSum(tree:List[Any]):Int ={
  val result = leafSumImpl(0,tree)
  result
}

val list1 = List(List(3,8),2,List(5))

println(leafSum(list1))