
def largest(fun: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(fun(_)).max
}

val result = largest(x => 10 * x - x * x, 1 to 10)

println(result)//25

/*
10-1
10*2-4

10*5 -5*5 =25

10*10 -10*10 =0
 */