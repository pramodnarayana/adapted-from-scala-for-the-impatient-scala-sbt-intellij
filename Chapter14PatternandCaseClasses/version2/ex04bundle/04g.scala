
sealed abstract class Item

case class Article(desc:String,p:Double) extends Item
case class Bundle(desc:String, discount:Double,items:Item*) extends Item
case class Multiple(q:Int, item:Item) extends Item

def price(item:Item):Double =item match {
  case Article(_,p)=>p
  case Multiple(q,i)=>q*price(i)
  case Bundle(_,d,items @_*)=> items.map(price _).sum- d
}


val x = Bundle("Father's day special", 20.0,
  Multiple(1, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0,
    Article("Old Potrero Straight Rye Whiskey", 79.95),
    Article("Junípero Gin", 32.95)))

println(price(x)) //122.85000000000002


