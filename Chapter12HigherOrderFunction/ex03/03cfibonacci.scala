
def factorial(n: Int): Int = {
  (1 to n).reduceLeft((_*_))
}

val a = factorial(5)

println(a)//120

def fibo(n:Int):Int ={
  def fiboIter(i:Int,a:Int,b:Int) :Int= if(i==n)a else fiboIter(i+1,b,a+b)
fiboIter(0,0,1)
}

val b = fibo(10)


println(b)//55