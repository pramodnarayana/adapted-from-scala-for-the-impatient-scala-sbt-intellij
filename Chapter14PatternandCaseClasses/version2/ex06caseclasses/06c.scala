/*
6.
May 24, 2015
 */


sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

private def sumLeafImpl(tree: BinaryTree): Int = tree match {
  case Node(l, r) => sumLeafImpl(l) + sumLeafImpl(r)
  case Leaf(x) => x
}

def sumLeaf(tree: BinaryTree): Int = {
  val result = sumLeafImpl(tree)
  result
}

val data = Node(Node(Leaf(1), Leaf(5)), Node(Leaf(3), Leaf(9)))

println(sumLeaf(data)) //18