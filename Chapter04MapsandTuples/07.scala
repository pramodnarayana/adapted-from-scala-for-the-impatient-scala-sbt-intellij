import scala.collection.JavaConversions.propertiesAsScalaMap

def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

val props = propertiesAsScalaMap(System.getProperties())

val maxkey = props.keySet.map(_.length).max

for( (k,v) <- props ) printf("%-" + maxkey + "s | %s\n", k, v)

/*
April 12, 2015

scala 07.scala
env.emacs                     |
java.runtime.name             | Java(TM) SE Runtime Environment
sun.boot.library.path         | /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib
java.vm.version               | 25.40-b25
gopherProxySet                | false
java.vm.vendor                | Oracle Corporation
java.vendor.url               | http://java.oracle.com/
path.separator                | :
java.vm.name                  | Java HotSpot(TM) 64-Bit Server VM
file.encoding.pkg             | sun.io
user.country                  | US
sun.java.launcher             | SUN_STANDARD
sun.os.patch.level            | unknown
java.vm.specification.name    | Java Virtual Machine Specification
user.dir                      | /Users/davidtan/Documents/coursera/scala-impatient/reference/hempalex/Chapter04MapandTuples
java.runtime.version          | 1.8.0_40-b27
java.awt.graphicsenv          | sun.awt.CGraphicsEnvironment
java.endorsed.dirs            | /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/endorsed
os.arch                       | x86_64
java.io.tmpdir                | /var/folders/33/j5kgs6x16t1g7xm9qs0sf6w40000gn/T/
line.separator                |

java.vm.specification.vendor  | Oracle Corporation
os.name                       | Mac OS X
sun.jnu.encoding              | UTF-8
java.library.path             | /Users/davidtan/Library/Java/Extensions:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java:.
java.specification.name       | Java Platform API Specification
java.class.version            | 52.0
sun.management.compiler       | HotSpot 64-Bit Tiered Compilers
os.version                    | 10.10
http.nonProxyHosts            | local|*.local|169.254/16|*.169.254/16
user.home                     | /Users/davidtan
user.timezone                 | America/Chicago
scala.home                    | /usr/local/Cellar/scala/2.11.4/libexec
java.awt.printerjob           | sun.lwawt.macosx.CPrinterJob
file.encoding                 | UTF-8
java.specification.version    | 1.8
scala.usejavacp               | true
java.class.path               | ""
user.name                     | davidtan
java.vm.specification.version | 1.8
sun.java.command              | scala.tools.nsc.MainGenericRunner 07.scala
java.home                     | /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre
sun.arch.data.model           | 64
user.language                 | en
java.specification.vendor     | Oracle Corporation
awt.toolkit                   | sun.lwawt.macosx.LWCToolkit
java.vm.info                  | mixed mode
java.version                  | 1.8.0_40
java.ext.dirs                 | /Users/davidtan/Library/Java/Extensions:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/ext:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java
sun.boot.class.path           | /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/resources.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/rt.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/sunrsasign.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/jsse.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/jce.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/charsets.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/lib/jfr.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/jre/classes:/usr/local/Cellar/scala/2.11.4/libexec/lib/akka-actor_2.11-2.3.4.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/config-1.2.1.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/jline-2.12.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-actors-2.11.0.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-actors-migration_2.11-1.1.0.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-compiler.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-continuations-library_2.11-1.0.2.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-continuations-plugin_2.11.4-1.0.2.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-library.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-parser-combinators_2.11-1.0.2.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-reflect.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-swing_2.11-1.0.1.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scala-xml_2.11-1.0.2.jar:/usr/local/Cellar/scala/2.11.4/libexec/lib/scalap-2.11.4.jar
java.vendor                   | Oracle Corporation
file.separator                | /
java.vendor.url.bug           | http://bugreport.sun.com/bugreport/
sun.io.unicode.encoding       | UnicodeBig
sun.cpu.endian                | little
socksNonProxyHosts            | local|*.local|169.254/16|*.169.254/16
ftp.nonProxyHosts             | local|*.local|169.254/16|*.169.254/16
sun.cpu.isalist               |
Davids-MacBook-Pro:Chapter04MapandTuples davidtan$
 */