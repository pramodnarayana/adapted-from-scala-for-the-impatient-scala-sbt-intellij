def swap03(arr: Array[Int]) = arr match {
  case Array(x, y, xs@_*) => Array(y, x) ++ xs
  case _ => arr
}

val arr1 = Array(1, 2, 4, 5)

val result = swap03(arr1)

println(arr1.mkString(","))

println(result.mkString(","))