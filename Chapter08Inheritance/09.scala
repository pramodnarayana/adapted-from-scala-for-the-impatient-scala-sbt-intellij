
//package Chapter08Inheritance

class Creature {
  def range: Int = 10

  val env: Array[Int] = new Array[Int](range)
}

class Ant extends Creature {
  override def range = 2
}

val a = new Ant
println(a.range) //2
println(a.env.length) //0  if def-> 2