/*


 ++ -> Array
 ::: List
 */

private def leafsumImpl(store_total: Int, nodes: List[Any]): Int = nodes match {
  case List() => store_total
  case head :: tail => head match {
    case x: Int => leafsumImpl(x + store_total, tail)
    case y: List[Any] => leafsumImpl(store_total, y ::: tail)
    case _ => store_total
  }
}
def leafSum(t:Int,n:List[Any]):Int ={
  val x = leafsumImpl(t,n)
  x
}


val list1 = List(List(3,8),2,List(5))

println(leafSum(0,list1))