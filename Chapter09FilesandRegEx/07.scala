

val tokens = io.Source.fromFile("07.txt").mkString.split("""\s+""").filter(
	"^[0-9]+(\\.[0-9]+)?$".r findFirstIn _ match {
		case Some(_) => false
		case None => true
	}
)

println("Tokents: " + tokens.mkString(", "))

/*
Tokents: lkjdlkjln, dkhkjhd, dlkj, djjd, ""d, djdh7883, 3., .33


 */


/*
INPUT FILE
lkjdlkjln dkhkjhd
dlkj 7474.44 djjd
""d djdh7883 7373.11
2 1
3.
333
.33
 */

/*
^[0-9]+(\.[0-9]+)?$

/^[0-9]+(\.[0-9]+)?$/
^ assert position at start of the string
[0-9]+ match a single character present in the list below
Quantifier: + Between one and unlimited times, as many times as possible, giving back as needed [greedy]
0-9 a single character in the range between 0 and 9
1st Capturing group (\.[0-9]+)?
Quantifier: ? Between zero and one time, as many times as possible, giving back as needed [greedy]
Note: A repeated capturing group will only capture the last iteration. Put a capturing group around the repeated group to capture all iterations or use a non-capturing group instead if you're not interested in the data
\. matches the character . literally
[0-9]+ match a single character present in the list below
Quantifier: + Between one and unlimited times, as many times as possible, giving back as needed [greedy]
0-9 a single character in the range between 0 and 9
$ assert position at end of the string

 */