def multiDim(a: Array[Int], dim: Int) = a.grouped(dim).toArray.map(_.toArray)

val a = (1 to 10).toArray

val b = multiDim(a, 3)


println(b.deep.mkString(","))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 08bMultiDim.scala
Array(1, 2, 3),Array(4, 5, 6),Array(7, 8, 9),Array(10)
Davids-MacBook-Pro:byMe davidtan$

 */