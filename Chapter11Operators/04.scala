//http://like-a-boss.net/2012/07/30/ordering-and-ordered-in-scala.html
//http://www.scala-lang.org/api/2.11.5/index.html#scala.math.Ordered


/* DOLLAR AND SENSE */

class Money(d: BigInt, c: BigInt) extends Ordered[Money] {
  val dollars:BigInt = d + c/100
  val cents:BigInt = c%100 //NOT this   d*100 +c
  def toCents():BigInt = dollars*100 +cents
  def fromCents(cents:BigInt)= new Money(cents/100,cents%100)
  def +(that:Money) = fromCents(this.toCents + that.toCents)
  def -(that:Money) = fromCents(this.toCents - that.toCents)


  override def toString() = "%d.%d".format(dollars,cents)
  override def compare(that:Money) = toCents.compare(that.toCents)
}

object Money {
def apply(d:Int,c:Int) = new Money(d,c)
}
val x = Money(1,75)
val y = Money(0,50)
println(x+y) //2.25
println(x-y)//1.25
println(x>y)//true
