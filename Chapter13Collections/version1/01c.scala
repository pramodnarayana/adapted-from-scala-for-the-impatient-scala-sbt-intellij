import scala.collection.mutable


def indexes(s:String)={
  val res = new mutable.HashMap[Char,mutable.LinkedHashSet[Int]]()
  for((c,index)<-s.zipWithIndex){
    val set = res.getOrElse(c.toChar, new mutable.LinkedHashSet[Int])
    set +=index  //Modifies coll by addin g
    println(set)
    res(c.toChar) = set
    println(res)
  }
  res  // return
}

val x = "Mississippi"

println(indexes(x))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 01c.scala
Set(0)
Map(M -> Set(0))
Set(1)
Map(M -> Set(0), i -> Set(1))
Set(2)
Map(M -> Set(0), s -> Set(2), i -> Set(1))
Set(2, 3)
Map(M -> Set(0), s -> Set(2, 3), i -> Set(1))
Set(1, 4)
Map(M -> Set(0), s -> Set(2, 3), i -> Set(1, 4))
Set(2, 3, 5)
Map(M -> Set(0), s -> Set(2, 3, 5), i -> Set(1, 4))
Set(2, 3, 5, 6)
Map(M -> Set(0), s -> Set(2, 3, 5, 6), i -> Set(1, 4))
Set(1, 4, 7)
Map(M -> Set(0), s -> Set(2, 3, 5, 6), i -> Set(1, 4, 7))
Set(8)
Map(M -> Set(0), s -> Set(2, 3, 5, 6), p -> Set(8), i -> Set(1, 4, 7))
Set(8, 9)
Map(M -> Set(0), s -> Set(2, 3, 5, 6), p -> Set(8, 9), i -> Set(1, 4, 7))
Set(1, 4, 7, 10)
Map(M -> Set(0), s -> Set(2, 3, 5, 6), p -> Set(8, 9), i -> Set(1, 4, 7, 10))
()
Davids-MacBook-Pro:byMe davidtan$

 */