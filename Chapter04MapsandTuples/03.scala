

import scala.io.Source

def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

val source = Source.fromFile("gpl.txt", "UTF-8")
val tokens = source.mkString.split("\\s+")

var freq = new scala.collection.immutable.HashMap[String, Int]

tokens foreach { token =>
	freq = freq + (token -> (freq.getOrElse(token, 0) + 1) )
}

prn(freq)
/*
avids-MacBook-Pro:Chapter04MapsandTuples davidtan$ scala 03.scala
HashTrieMap(comply -> 1, assert -> 1, Copies. -> 2, physically -> 1, language, -> 1, used -> 10, allowed -> 1,
 widely -> 1, IN -> 3, library. -> 1, procuring -> 1, recipient -> 2, "source -> 1, fashion -> 1,
 interfered -> 1, Product" -> 1, misrepresentation -> 1, obligate -> 1, warranties -> 1, enable -> 1,
  entirely -> 1, measure -> 1, stating -> 4, adapt -> 1, Legal -> 5, notify -> 1, versions. -> 1, rega
 */