
import collection.mutable._
import java.io._

class Person(val name: String) extends Serializable {
  private val friends = new ArrayBuffer[Person]

  def friend(f: Person) = friends += f

  override def toString = "%s {%s}".format(name, friends.map(_.name).mkString(", "))
}

object Person{
  def apply(name :String)= new Person(name)
}


val john = new Person("Jon")
val mike = new Person("Michael")
val matt = new Person("Matthew")
val devin = new Person("Devin")

john friend mike
mike friend matt
matt friend john
devin friend john
devin friend mike
devin friend devin

val all = Array(john, mike, matt, devin)

println("Original objects are", all.mkString(","))
/*
scala> res7: scala.collection.mutable.ArrayBuffer[Person] =
 ArrayBuffer(Jon {Michael}, Michael {Matthew}, Devin {Jon, Michael, Devin})

 */

val out = new ObjectOutputStream(new FileOutputStream("10.txt"))
out.writeObject(all)
out.close()

val in = new ObjectInputStream(new FileInputStream("10.txt"))
val res = in.readObject().asInstanceOf[Array[Person]]
in.close()

println("Restored objects: " + res.mkString(", "))//Restored objects: Jon {Michael}, Michael {Matthew}, Matthew {Jon}, Devin {Jon, Michael, Devin}


/*
davidtan$ scala -Xnojline < 10.scala
 */