
class Suits extends Enumeration {
  type Suits = Value
  val Spade = Value("♠")
  val Club = Value("♣")
  val Heart = Value("♥")
  val Diamond = Value("♦")
}

val a = new Suits

for( v <- a.values)println("V is", a, "and ", v)