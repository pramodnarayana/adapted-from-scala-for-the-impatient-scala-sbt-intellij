

//import collection.immutable.List

abstract class Item {
  def desc: String

  def price: Double

  override def toString = "%s(%s , %f)".format(this.getClass.getSimpleName, desc, price)
}

class SimpleItem(
                  override val desc: String,
                  override val price: Double
                  ) extends Item

class Bundle extends Item {
  private var items: List[Item] = List()

  def add(item: Item) = {
    items = item :: items
  }

  def price: Double = items.map(_.price).sum

  def desc: String = items.map(_.desc).mkString(",")
}

var l:List[Item] = List(
  new SimpleItem("xsdf", 1000),
  new SimpleItem("sdfvg", 1020)
)

val b = new Bundle
b.add(new SimpleItem("dsfg", 999))
b.add(new SimpleItem("dsfgdh", 2134))


l = b :: l


l.foreach(println(_))
