def func(x: Double, n: Int): Double = {
  if (n == 0) 1
  else {
    if (n > 0) {
      if (n % 2 == 0 && n > 2) {
        func(func(x, n / 2), 2)
      } else {
        // < 0
        x * func(x, n - 1)
      }
    } else (1 / func(x, -n))
  }
}

printf("2^2=%f\n", func(2, 2)); // 2 to the power 2
printf("10^9=%f\n", func(10, 9));
printf("3^3=%f\n", func(3, 3));
printf("2^-1=%f\n", func(2, -1));
printf("5^0=%f\n", func(5, 0));

/*
Davids-MacBook-Pro:Chapter02 davidtan$ scala -Xnojline < 10.scala
Welcome to Scala version 2.11.4 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_25).
Type in expressions to have them evaluated.
Type :help for more information.

scala>      |      |      |      |      |      |      |      |      |      |      | func: (x: Double, n: Int)Double

scala>
scala> 2^2=4.000000

scala> 10^9=1000000000.000000

scala> 3^3=27.000000

scala> 2^-1=0.500000

scala> 5^0=1.000000

scala> :quit

 */