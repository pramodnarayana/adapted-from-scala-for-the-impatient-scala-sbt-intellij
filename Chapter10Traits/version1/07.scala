

trait Animal{
	val name:String
	def words:String =""
	def say = println("%s: %s".format(name,words))
}

trait Endothermy

trait Mammal extends Animal with Endothermy

class Dog extends Mammal{
	val name ="Scooby"
	override val words ="BowWow"
	override def say ={
		print("Dog name is:")
		super.say
	}
}
class Cat extends Mammal{
	val name ="Tom"
	override val words ="MeowScratchScratch"
	override def say ={
		print("Cat name is:")
		super.say
	}
}
val d = new Dog()
val c = new Cat()

d.say
c.say
/*
Davids-MacBook-Pro:byMe davidtan$ scala 07.scala
Dog name is:Scooby: BowWow
Cat name is:Tom: MeowScratchScratch
 */