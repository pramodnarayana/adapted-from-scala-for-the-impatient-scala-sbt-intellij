/*

June 23


 lst2 ::: 1st  given list prepend to another lst
 */

def leafsumImp(total:Int, nodes:List[Any]):Int = nodes match{
  case List()=>total
  case h::t => h match {
    case x:Int => leafsumImp(x+total, t)
    case y:List[Any]=> leafsumImp(total, y:::t)
    case _ => total
  }
}

def leafsum(l:List[Any]):Int ={
  val result = leafsumImp(0,l)
  result
}
val list1 = List(List(3,8),2,List(5))

println(list1)
println(leafsum(list1))