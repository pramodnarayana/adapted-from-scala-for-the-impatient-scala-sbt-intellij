/*

May 22, 2015
 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(l, r) => leafsum(l) + leafsum(r)
  case Leaf(x) => x
}

val tree01 = Node(Leaf(12), Node(Node(Leaf(3), Leaf(8)), Node(Leaf(5), Leaf(9))))

println(tree01)

println(leafsum(tree01))