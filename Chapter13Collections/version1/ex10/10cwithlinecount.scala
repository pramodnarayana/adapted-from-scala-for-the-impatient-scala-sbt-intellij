//  val lines2 = scala.io.Source.fromFile("someFile.txt").getLines()
 //   val count2 = lines2.map(x => 1).sum


val str = io.Source.fromFile("10.txt").mkString

// mutable
val freq01 = new scala.collection.mutable.HashMap[Char, Int]
for (c <- str) freq01(c) = freq01.getOrElse(c, 0) + 1
//println(freq01.toSeq.sorted)


val freq02 = str.map(c => (c, 1)).groupBy(_._1).map(x => (x._1, x._2.length))
println(freq02.toSeq)

val lines2 = scala.io.Source.fromFile("10.txt").getLines()
val count2 = lines2.map(x => 1).sum


println(count2," LINES")