import scala.util.Random

val a = new Array[Int](10).map(x=> Random.nextInt(100))

println(a.mkString(","))

def getMaxRecursive(pos :Int, arr:Array[Int]):Int ={
  if(pos == arr.length-1) arr(pos) else Math.max(arr(pos), getMaxRecursive(pos+1,arr))
}

val b = getMaxRecursive(0,a)

println(b)