
trait Animal {
  val name: String

  def words: String = ""

  def say = println("%s: I make this sound %s".format(name, words))
}

trait Endothermy

trait Mammal extends Animal with Endothermy

class Dog extends Mammal {
  override val name = "Scooby"

  override def words = "BowWow"

  override def say = {
    print("I am")
    super.say
  }

}

val d = new Dog

d.say

