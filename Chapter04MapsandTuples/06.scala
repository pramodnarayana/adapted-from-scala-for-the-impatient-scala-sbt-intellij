
import java.util.Calendar._

val days = scala.collection.mutable.LinkedHashMap(
	"MONDAY" -> MONDAY,
	"TUESDAY" -> TUESDAY,
	"WEDNESDAY" -> WEDNESDAY,
	"THURSDAY" -> THURSDAY,
	"FRIDAY" -> FRIDAY,
	"SATURDAY" -> SATURDAY,
	"SUNDAY" -> SUNDAY
)

for ( (k, v) <- days ) printf("%s=%d\n", k, v)

/*
Davids-MacBook-Pro:Chapter04MapsandTuples davidtan$ scala 06.scala
MONDAY=2
TUESDAY=3
WEDNESDAY=4
THURSDAY=5
FRIDAY=6
SATURDAY=7
SUNDAY=1

 */