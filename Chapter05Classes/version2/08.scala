

class Car(val manufacturer: String, val model_name: String, val model_year: Int = -1, var license: String = "") {

  override def toString = "Car(%s %s %d %s)".format(manufacturer, model_name, model_year, license)
}

val c1 = new Car("Honda", "Civic", 2011, "xx123zz")
println(c1)