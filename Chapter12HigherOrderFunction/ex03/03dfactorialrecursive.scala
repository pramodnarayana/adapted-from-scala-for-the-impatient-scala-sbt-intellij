
def factorial(n:Int):Int={
  if(n<=0)1 else n*factorial(n-1)
}

val a = factorial(5) //120

println(a)