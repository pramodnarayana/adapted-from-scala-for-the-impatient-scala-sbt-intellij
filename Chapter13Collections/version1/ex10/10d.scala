

val str = io.Source.fromFile("10.txt").mkString
val frequencies = new scala.collection.mutable.HashMap[Char, Int]
for (c <- str) frequencies(c) = frequencies.getOrElse(c, 0) + 1

//println(frequencies.toSeq)

val f2 = str.map(c=>(c,1)).groupBy(_._1).map(x=>(x._1,x._2.length))

println(f2.toSeq)