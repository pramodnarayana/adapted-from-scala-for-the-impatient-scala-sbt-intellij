object Name{
def unapplySeq(s:String):Option[Seq[String]] =
  if(s.trim=="")None else Some(s.trim.split("\\s+"))
}
val author ="Peter van der Linden"

val x = author match {
  case Name(first,last) =>author
  case Name(first,middle,last)=>first+" "+last
  case Name(first,"van","der",last)=>"Hello PeterLee!"
}

println(x)