/*
6.
May 22, 2015
June 22, 2015
 */


sealed abstract class BTree
case class Leaf(x:Int)extends BTree
case class Node(leaf:BTree,right:BTree)extends BTree

def sumTree(t:BTree):Int= t match{
  case Leaf(x:Int)=>x
  case Node(l,r)=>sumTree(l)+sumTree(r)
}

val tree01 = Node(Leaf(12), Node(Node(Leaf(3), Leaf(8)), Node(Leaf(5), Leaf(9))))

println(sumTree(tree01))//37
