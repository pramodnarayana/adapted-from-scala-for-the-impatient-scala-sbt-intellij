//package Chapter09FilesandRegEx

import collection.mutable._
import java.io._

class Person(val name: String) extends Serializable {
  private val friends = new ArrayBuffer[Person]

  // some kind of DSL :-)
  def friend(f: Person) = friends += f

  override def toString = "%s {%s}".format(name, friends.map(_.name).mkString(", "))
}

object Person {
  def apply(name: String) = new Person(name)
}

//object Main {

val anna = Person("Anna")
val boris = Person("Boris")
val clair = Person("Clair")

anna friend boris
boris friend anna
anna friend clair
clair friend boris

val all = Array(anna, boris, clair)

println("Original objects: " + all.mkString(", "))

val out = new ObjectOutputStream(new FileOutputStream("10.txt"))
out.writeObject(all)
out.close()

val in = new ObjectInputStream(new FileInputStream("10.txt"))
val res = in.readObject().asInstanceOf[Array[Person]]
in.close()

println("Restored objects: " + res.mkString(", "))
//}

/*
-MacBook-Pro:Chapter09FilesandRegEx davidtan$ scala 10.scala
Original objects: Anna {Boris, Clair}, Boris {Anna}, Clair {Boris}

 */