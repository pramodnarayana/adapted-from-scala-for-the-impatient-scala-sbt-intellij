val source = io.Source.fromFile("mary.txt")
var temp: Int = 0
var count: Int = 1 // starts from line 1
val result = source.getLines.mkString.split( """[^a-zA-Z\-]+""").map(x => x match {
  case ("\n") => (x, count += 1)
  case _ => (x, count)
})

println(result.mkString(","))