import scala.collection.mutable._

def indexes(s: String) = {
  var res = new HashMap[Char, LinkedHashSet[Int]]() // this is mutable
  for ((c, i) <- s.zipWithIndex) {

    val set = res.getOrElse(c.toChar, new LinkedHashSet[Int])
    set += i
    res(c.toChar) = set
  }

  res
}

val a = "Mississippi"

println(indexes(a))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 01bIndexes.scala
Map(M -> Set(0), s -> Set(2, 3, 5, 6), p -> Set(8, 9), i -> Set(1, 4, 7, 10))
Davids-MacBook-Pro:byMe davidtan$
 */