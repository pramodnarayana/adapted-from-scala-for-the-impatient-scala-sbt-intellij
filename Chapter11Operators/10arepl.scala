object Name {
  def unapplySeq(input: String): Option[Seq[String]] =
    if (input.trim == "") None else Some(input.trim.split("\\s+"))

}

val author = "Peter Linden"//van der

val x = author match {
  case Name(first, last) => author
  case Name(first, middle, last) => first + " " + last
  case Name(first, "van", "der", last) => "Hello Peter!"
}


println(x)
