def fibo(n: Int): Int = {
  def fiboIter(i: Int, a: Int, b: Int): Int = if (n == i) a else fiboIter(i + 1, b, a + b)
  fiboIter(0, 0, 1)
}

val a = fibo(10)
println(a) //55


def fib(n: Int): Int = {
  // bad fibo do not use mutable
  var a = 0
  var b = 1
  var i = 0
  while (i < n) {
    val prev_a = a
    a = b
    b = prev_a + b
    i = i + 1
    println(a)

  }
  a
}

val c = fib(10)
println(c) //55

// streams list
val fibs: Stream[BigInt] =
  0 #:: 1 #:: (fibs zip fibs.tail).map { case (a, b) => a + b }


println(fibs(10)) //55

val fibs2: Stream[BigInt] =
  1 #:: (fibs2 zip fibs2.tail).map {
    case (a, b) => a + b
  }

println("fibs2 ", fibs(10))//fibs2, 55

//val d: Stream[Int] = 0 #:: 1 # ::

def fibN(n: Int): Int = {if (n < 3) 1 else fibN(n - 1) + fibN(n - 2)}

val e = fibN(10)

println(e) //55


def fibF(n : Int): Int = if (n <2) n else fibF(n-1) + fibF(n-2)
println(fibF(10))//55
//1, 1, 2, 3, 5, 8, 13, 21
//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55